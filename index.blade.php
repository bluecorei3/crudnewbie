@extends('admin-LTE.app')

@section('title', 'Category-Index')

@section('content')
    <button ><a href="{{url('/kategori/create')}}">Tambah Kategori</a></button><br><br>
    <table class="table table-bordered">
        <tr>
            <th>No. </th>
            <th>Nama Kategori</th>
            <th>Aksi</th>
        </tr>

   @foreach($semua_kategori as $kategori)
    <tr>
        <td>{{$loop->iteration}}</td>
        <td>{{$kategori->nama_kategori}}</td>
        <td>
          <a class="btn btn-info btn-xs" href="{{ url('/kategori/' . $kategori->id . '/edit')}}">Edit</a>
            <a class="btn btn-success btn-xs" href="{{ url('/kategori/' . $kategori->id . '/show')}}"><i class="fa fa-fw fa-eye"></i> </a>
            <form action="{{ url('/kategori/' . $kategori->id . '/delete')}}" method="post" style="display:inline">
              {{ csrf_field() }}
              {{ method_field('DELETE') }}
              <input type="submit" class="btn  btn-danger btn-xs" value="delete">
            </form>
        </td>
    </tr>
   @endforeach
   </table>
@endsection
