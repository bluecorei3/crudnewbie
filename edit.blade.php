@extends('admin-LTE.app')

@section('title', 'Category-Edit')

@section('content')
    <form class="" action="{{url('kategori/'.$kategori->id.'/edit')}}" method="post">
        <input type="text" class="form-control" name="nama_kategori" 
        placeholder= " isi nama kategori" value= "{{$kategori->nama_kategori}}" id=""><br>
        {{csrf_field()}}
        {{method_field('PUT')}}
        <input type="submit" class= "button btn-success" name="" value="Update Kategori">
    </form>
    
@endsection